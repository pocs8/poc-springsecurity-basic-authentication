package com.gitlab.wesleyosantos91.backend.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : wesleyosantos91
 * @Date : 15/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@RestController
@RequestMapping("/hello-world")
public class HelloWorld {

    @GetMapping
    public String helloWorld() {
        return "Hello World";
    }

}
