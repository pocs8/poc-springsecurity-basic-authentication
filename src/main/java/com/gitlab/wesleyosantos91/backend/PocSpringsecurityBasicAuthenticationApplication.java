package com.gitlab.wesleyosantos91.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocSpringsecurityBasicAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocSpringsecurityBasicAuthenticationApplication.class, args);
	}

}
