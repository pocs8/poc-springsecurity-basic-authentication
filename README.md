# Poc - Basic Authentication com Spring Security

## Prova de conceito - Basic Authentication com Spring Security

> A POC demonstra como criar uma autenticação básica com a API Spring Security para REST usando o Spring Boot.

# Tecnologias
- Java 11
- Spring Boot 2.3.0.RELEASE
    - spring-boot-starter-web
    - spring-boot-devtools
    - spring-boot-starter-security
- Tomcat (Embedded no Spring Boot)
- Git

# Execução

A execução das aplicações são feitas através do de um comando Maven que envoca a inicialização do Spring Boot.

- Scripts
    - ```./mvnw clean spring-boot:run```
    
# Utilização

- curl --location --request GET 'http://localhost:8080/hello-world' \
--header 'Authorization: Basic d2VzbGV5b3NhbnRvczkxOnNwcmluZw=='

- curl -X GET --user wesleyosantos91:spring "http://localhost:8080/hello-world"